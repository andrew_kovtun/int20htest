package com.itv.int20htest.Models;

import android.support.annotation.NonNull;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

public class NBUService {
    private final static AsyncHttpClient client = new AsyncHttpClient();

    public static void runCurrencyRequest(@NonNull FileAsyncHttpResponseHandler listener, int currency, @NonNull String startDate, @NonNull String endDate) {
        String url = constructRequestLink(currency, startDate, endDate);
        client.get(url, listener);
    }

    @NonNull
    private static String constructRequestLink(int currency, @NonNull String startDate, @NonNull String endDate) {
        return new StringBuilder().append("https://bank.gov.ua/control/uk/curmetal/currency/search?formType=searchPeriodForm&time_step=daily&currency=")
                .append(currency)
                .append("&periodStartTime=")
                .append(startDate)
                .append("&periodEndTime=")
                .append(endDate)
                .append("&outer=xml").toString();
    }

}
