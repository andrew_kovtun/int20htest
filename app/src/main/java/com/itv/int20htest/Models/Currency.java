package com.itv.int20htest.Models;

import java.util.Date;

public class Currency
{
    public final Date date;
    public final String letterCode;
    public final String name;
    public final Double exchangeRate;

    public Currency(Date date, String letterCode, String name, Double exchangeRate) {
        this.date = date;
        this.letterCode = letterCode;
        this.name = name;
        this.exchangeRate = exchangeRate;
    }
}
