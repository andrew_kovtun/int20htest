package com.itv.int20htest.Presenters;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.itv.int20htest.Models.Currency;
import com.itv.int20htest.Models.NBUService;
import com.itv.int20htest.Views.StartViewCallback;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cz.msebera.android.httpclient.Header;

public class StartPresenter implements StartPresenterCallback
{
  private StartViewCallback viewCallback;
  private String startDate;
  private String endDate;
  private int currencyCode;

  @Override
  public void editedStartDate(@NonNull String startDate)
  {
    this.startDate = startDate;
  }

  @Override
  public void editedEndDate(@NonNull String endDate)
  {
    this.endDate = endDate;
  }

  @Override
  public void editedCurrency(int currencyCode)
  {
    this.currencyCode = currencyCode;
  }


  @Override
  public void goToAction()
  {
    if (viewCallback != null) {
      if (!isEditsValid())
        viewCallback.showError("Values in edits are not valid!");
      else {
        if (viewCallback.isNetworkAvailable())
          NBUService.runCurrencyRequest(new FileAsyncHttpResponseHandler(viewCallback.getContext())
          {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file)
            {
              if (viewCallback != null) {
                viewCallback.showError("Could not access");
                viewCallback.hideProgress();
                viewCallback.enableSearch();
              }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file)
            {
              viewCallback.hideProgress();
              viewCallback.enableSearch();
              List<Currency> currencyList = parseCurrenciesFrom(file);
              if (currencyList != null)
                viewCallback.showDataInGraph(currencyList);
              else viewCallback.showError("Could not refresh.");
            }

            @Override
            public void onStart()
            {
              if (viewCallback != null) {
                viewCallback.showProgress();
                viewCallback.disableSearch();
              }
            }

            @Override
            public void onRetry(int retryNo)
            {
            }
          }, currencyCode, startDate, endDate);
        else viewCallback.showError("Could not access!\nCheck internet connection.");
      }
    }
  }

  private boolean isEditsValid()
  {
    return startDate != null && endDate != null;
  }

  @Override
  public void setView(@NonNull StartViewCallback startViewCallback)
  {
    this.viewCallback = startViewCallback;
  }

  @Nullable
  private List<Currency> parseCurrenciesFrom(@NonNull File file)
  {
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = null;
    List<Currency> currencyList = new ArrayList<>();
    try {
      dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(file);
      doc.getDocumentElement().normalize();
      NodeList nList = doc.getElementsByTagName("currency");
      for (int temp = 0; temp < nList.getLength(); temp++) {
        Node nNode = nList.item(temp);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nNode;
          String myFormat = "dd.MM.yyyy"; //In which you need put here
          SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
          Date date = sdf.parse(eElement.getElementsByTagName("date").item(0).getTextContent());
          String letterCode = eElement.getElementsByTagName("letter_code").item(0).getTextContent();
          String name = eElement.getElementsByTagName("currency_name").item(0).getTextContent();
          Double exchangeRate = Double.valueOf(eElement.getElementsByTagName("exchange_rate").item(0).getTextContent());
          exchangeRate /= 100;
          currencyList.add(new Currency(date, letterCode, name, exchangeRate));
        }
      }
      return currencyList;
    } catch (ParserConfigurationException | IOException | SAXException e) {
      return null;
    } catch (ParseException e) {
      return null;
    }
  }
}
