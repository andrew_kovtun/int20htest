package com.itv.int20htest.Presenters;

import android.support.annotation.NonNull;

import com.itv.int20htest.Views.StartViewCallback;

public interface StartPresenterCallback {

    void editedStartDate(@NonNull String startDate);

    void editedEndDate(@NonNull String endDate);

    void editedCurrency(int currencyCode);

    void goToAction();

    void setView(@NonNull StartViewCallback startViewCallback);
}
