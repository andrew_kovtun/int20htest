package com.itv.int20htest.Views;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.itv.int20htest.Models.Currency;
import com.itv.int20htest.Presenters.StartPresenter;
import com.itv.int20htest.Presenters.StartPresenterCallback;
import com.itv.int20htest.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class StartActivity extends AppCompatActivity implements StartViewCallback
{
  private static final StartPresenterCallback presenter = new StartPresenter();
  private final Calendar myCalendar = Calendar.getInstance();
  private final DatePickerDialog.OnDateSetListener startDateListener = new DatePickerDialog.OnDateSetListener()
  {
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
      myCalendar.set(Calendar.YEAR, year);
      myCalendar.set(Calendar.MONTH, monthOfYear);
      myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
      updateStartLabel();
    }
  };
  private final DatePickerDialog.OnDateSetListener endDateListener = new DatePickerDialog.OnDateSetListener()
  {
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
      myCalendar.set(Calendar.YEAR, year);
      myCalendar.set(Calendar.MONTH, monthOfYear);
      myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
      updateEndLabel();
    }
  };
  private CurrencyAdapter currencyAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_start);
    findViewById(R.id.searchButton).setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View view)
      {
        presenter.goToAction();
      }
    });
    findViewById(R.id.startPeriod).setOnClickListener(
      new View.OnClickListener()
      {
        @Override
        public void onClick(View v)
        {
          new DatePickerDialog(StartActivity.this, startDateListener, myCalendar
            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
      });
    findViewById(R.id.endPeriod).setOnClickListener(
      new View.OnClickListener()
      {
        @Override
        public void onClick(View v)
        {
          new DatePickerDialog(StartActivity.this, endDateListener, myCalendar
            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
      });
    Spinner spinner = ((Spinner) findViewById(R.id.spinner));
    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
      this, R.layout.spinner_item, getResources().getStringArray(R.array.currencies)
    );
    spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
    spinner.setAdapter(spinnerArrayAdapter);
    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
    {
      public void onItemSelected(AdapterView<?> parent,
                                 View itemSelected, int selectedItemPosition, long selectedId)
      {
        int currencyCode = 0;
        switch (selectedItemPosition) {
          case 0:
            currencyCode = 169;
            break;
          case 1:
            currencyCode = 209;
            break;
          case 2:
            currencyCode = 196;
            break;
          case 3:
            currencyCode = 198;
            break;
        }
        presenter.editedCurrency(currencyCode);
      }

      public void onNothingSelected(AdapterView<?> parent)
      {
      }
    });
    currencyAdapter = new CurrencyAdapter(getBaseContext());
    ((ListView) findViewById(R.id.list_view)).setAdapter(currencyAdapter);
  }

  private void updateStartLabel()
  {
    String myFormat = "dd.MM.yyyy"; //In which you need put here
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    ((EditText) findViewById(R.id.startPeriod)).setText(sdf.format(myCalendar.getTime()));
    presenter.editedStartDate(sdf.format(myCalendar.getTime()));
  }

  private void updateEndLabel()
  {
    String myFormat = "dd.MM.yyyy"; //In which you need put here
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    ((EditText) findViewById(R.id.endPeriod)).setText(sdf.format(myCalendar.getTime()));
    presenter.editedEndDate(sdf.format(myCalendar.getTime()));
  }

  @Override
  protected void onStart()
  {
    super.onStart();
    presenter.setView(this);
  }

  @Override
  public void showProgress()
  {
    findViewById(R.id.progress).setVisibility(View.VISIBLE);
  }

  @Override
  public void hideProgress()
  {
    findViewById(R.id.progress).setVisibility(View.GONE);
  }

  @Override
  public void enableSearch()
  {
    findViewById(R.id.searchButton).setAlpha(1f);
    findViewById(R.id.searchButton).setEnabled(true);
  }

  @Override
  public void disableSearch()
  {
    findViewById(R.id.searchButton).setAlpha(0.7f);
    findViewById(R.id.searchButton).setEnabled(false);
  }

  @Override
  public void showDataInGraph(@NonNull List<Currency> currencyList)
  {
    currencyAdapter.clear();
    currencyAdapter.addAll(currencyList);
    currencyAdapter.notifyDataSetChanged();
  }

  @Override
  public boolean isNetworkAvailable()
  {
    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    return cm.getActiveNetworkInfo() != null;
  }

  @Override
  public void showError(@NonNull String message)
  {
    Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
  }

  @Override
  public Context getContext()
  {
    return getBaseContext();
  }

  private class CurrencyAdapter extends ArrayAdapter<Currency>
  {

    public CurrencyAdapter(Context context)
    {
      super(context, R.layout.currency_item);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
      Currency currency = getItem(position);
      if (convertView == null) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.currency_item, null);
      }
      if (currency != null) {
        ((TextView) convertView.findViewById(R.id.name)).setText(currency.name);
        ((TextView) convertView.findViewById(R.id.letter_code)).setText(currency.letterCode);
        ((TextView) convertView.findViewById(R.id.exchange_rate)).setText(String.valueOf(currency.exchangeRate));
        String myFormat = "dd.MM.yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        ((TextView) convertView.findViewById(R.id.date)).setText(sdf.format(currency.date));
      }
      return convertView;
    }
  }
}
