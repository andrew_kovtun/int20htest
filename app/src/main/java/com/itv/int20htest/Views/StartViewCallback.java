package com.itv.int20htest.Views;

import android.content.Context;
import android.support.annotation.NonNull;

import com.itv.int20htest.Models.Currency;

import java.util.List;

public interface StartViewCallback {

    void showProgress();

    void hideProgress();

    void enableSearch();

    void disableSearch();

    void showDataInGraph(@NonNull List<Currency> currencyList);

    boolean isNetworkAvailable();

    void showError(@NonNull String message);

    Context getContext();
}
